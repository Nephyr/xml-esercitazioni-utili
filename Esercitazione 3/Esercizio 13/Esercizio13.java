import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;
import java.util.*;
import java.text.*;

import java.io.File;
import java.io.IOException;

public class Esercizio13
{
    public static void main(String[] args)
    {
        XMLReader xmlr = new XMLReader();
        xmlr.Start();
    }
}

class XMLReader
{
    // Membri
    DocumentBuilderFactory _DOCFactory  = null;
    DocumentBuilder _DOCBuilder         = null;
    Document _DOC                       = null;
    
    Scanner _KEYS                       = null;
    NodeList _BOOKS                     = null;
    NodeList _USERS                     = null;
    
    public XMLReader()
    {
        try
        {    
            _DOCFactory = DocumentBuilderFactory.newInstance();
            _DOCFactory.setIgnoringElementContentWhitespace(true);
            
            _DOCBuilder = _DOCFactory.newDocumentBuilder();
            _DOC        = _DOCBuilder.parse("database_biblio.xml");
            
            _KEYS       = new Scanner(System.in);
        }
        catch(Exception e) 
        {
            System.out.println (e);
        }
    }
    
    public void Start()
    {
		System.out.println ("------------------------------------------------------------\n");

        Node doc        = _DOC.getFirstChild();
		Node root       = doc.getNextSibling();
        
        _BOOKS          = _DOC.getElementsByTagName("Libro");
		_USERS          = _DOC.getElementsByTagName("Utente");
        String buffer   = "";
        
        while(true)
        {
            System.out.print(" ->  Immettere un comando [ add | del | quit ]:  ");
            buffer = _KEYS.nextLine();
            
            if(buffer.equalsIgnoreCase("quit"))
                break;
            else 
            {
                if(buffer.equalsIgnoreCase("add"))
                {
                    AddLoan();
                }
                else if(buffer.equalsIgnoreCase("del"))
                {
                    DelLoan();
                }
                
                WriteXMLFile("./database_biblio.xml");
            }
        }

        System.out.println ("\n------------------------------------------------------------");
    }
    
    private void AddLoan()
    {
        boolean flag = true;
        String bufferU = "";
        String bufferL = "";
        System.out.print("\n");
           
        while(flag)
        {
            System.out.print("\t 1)  Immettere l'ID utente:  ");
            bufferU = _KEYS.nextLine();
            
            for(int i = 0; i < _USERS.getLength(); i++)
                if(((Element)_USERS.item(i)).getAttribute("id").equalsIgnoreCase(bufferU))
                {   
                    flag = false;
                    break;
                }
        }
        
        flag = true;
        
        while(flag)
        {
            System.out.print("\t 2)  Immettere l'ID libro:  ");
            bufferL = _KEYS.nextLine();
            
            for(int i = 0; i < _BOOKS.getLength(); i++)
                if(_BOOKS.item(i).getFirstChild().getFirstChild().getNodeValue().trim().equalsIgnoreCase(bufferL))
                {   
                    flag = false;
                    System.out.println("\n ->  Aggiunta prestito libro in corso...");
                    
                    Node prestito = _DOC.createElement("Prestito");
                    _BOOKS.item(i).appendChild( prestito );
                    
                    Element elem = (Element) prestito;
                    elem.setAttribute("idUtente", bufferU);
                    
                    Node dataPrestito = _DOC.createElement("DataPrestito");
                    prestito.appendChild( dataPrestito );
                    
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                    Date date = new Date();
                    
                    Node dataValPrestito = _DOC.createTextNode(dateFormat.format(date));
                    dataPrestito.appendChild( dataValPrestito );
                    
                    System.out.println(" ->  Aggiunta prestito libro completata con successo...\n");
                    break;
                }
        }
    }
    
    private void DelLoan()
    {
        boolean flag = true;
        String bufferU = "";
        String bufferL = "";
        System.out.print("\n");
           
        while(flag)
        {
            System.out.print("\t 1)  Immettere l'ID utente:  ");
            bufferU = _KEYS.nextLine();
            
            for(int i = 0; i < _USERS.getLength(); i++)
                if(((Element)_USERS.item(i)).getAttribute("id").equalsIgnoreCase(bufferU))
                {   
                    flag = false;
                    break;
                }
        }
        
        flag = true;
        
        while(flag)
        {
            System.out.print("\t 2)  Immettere l'ID libro:  ");
            bufferL = _KEYS.nextLine();
            
            for(int i = 0; i < _BOOKS.getLength(); i++)
                if(_BOOKS.item(i).getFirstChild().getFirstChild().getNodeValue().trim().equalsIgnoreCase(bufferL))
                {   
                    flag = false;
                    Node start = null;
                        
                    for (Node n = _BOOKS.item(i).getFirstChild(); n != null; n = n.getNextSibling())
                        if(n.getNodeName().equalsIgnoreCase("Prestito"))
                        {
                            start = n;
                            break;
                        }
                    
                    if(start != null)
                        for (Node n = start; n != null; n = n.getNextSibling())
                            if(((Element) n).getAttribute("idUtente").equalsIgnoreCase(bufferU))
                            {    
                                System.out.println("\n ->  Rimozione prestito libro in corso...");
                                n.getParentNode().removeChild(n);
                                System.out.println(" ->  Rimozione prestito libro completata con successo...\n");
                                
                                break;
                            }
                            else System.out.println("\n ->  L'utente '" + bufferU + "' non ha prestiti su libro '" + bufferL + "' attivi!\n");
                    else 
                        System.out.println("\n ->  Il libro '" + bufferL + "' non presenta prestiti attivi!\n");
                        
                    break;
                }
        }
    }
    
    private void WriteXMLFile(String filename) 
    {
        try {
            DOMSource source = new DOMSource(_DOC);
 
            File file = new File(filename);
            StreamResult result = new StreamResult(file);
 
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.transform(source, result);
        } 
        catch (TransformerConfigurationException e) {} 
        catch (TransformerException e) {}
    }

    private void AddNewNode( Node root )
    {
        // Nodo: LIBRO
        Node libro = _DOC.createElement("Libro");
        root.appendChild( libro );
        
        // Nodo: CODICE
        Node codice = _DOC.createElement("Codice");
        libro.appendChild( codice );
        Node ctext = _DOC.createTextNode("0103");
        codice.appendChild(ctext);
        
        // Nodo: TITOLO
        Node titolo = _DOC.createElement("Titolo");
        libro.appendChild( titolo );
        Node ttext = _DOC.createTextNode("Design Patterns Elements of Reusable ObjectOriented Software");
        titolo.appendChild(ttext);
        
        Map<String, String> _MapAuthor = new HashMap<String, String>();
        _MapAuthor.put("Erich Gamma", "Principale");
        _MapAuthor.put("Richard Helm", "Coautore");
        _MapAuthor.put("Ralph Johnson", "Coautore");
        _MapAuthor.put("John Vlissides", "Coautore");
        
        Iterator it = _MapAuthor.entrySet().iterator();
        
        while (it.hasNext())
        {
            Map.Entry _map  = (Map.Entry) it.next();
            String key      = (String) _map.getKey();
            String val      = (String) _map.getValue();
            
            // Nodo: AUTORE
            Node autore = _DOC.createElement("Autore");
            libro.appendChild( autore );
            
            Node atext = _DOC.createTextNode(key);
            autore.appendChild(atext);
            
            Element elem = (Element) autore;
            elem.setAttribute("Tipo", val);
        }
    }
}