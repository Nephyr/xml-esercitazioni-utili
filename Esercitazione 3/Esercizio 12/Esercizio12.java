import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;
import java.util.*;

import java.io.File;
import java.io.IOException;

public class Esercizio12
{
    public static void main(String[] args)
    {
        XMLReader xmlr = new XMLReader();
        xmlr.Start();
    }
}

class XMLReader
{
    // Membri
    DocumentBuilderFactory _DOCFactory  = null;
    DocumentBuilder _DOCBuilder         = null;
    Document _DOC                       = null;
    
    public XMLReader()
    {
        try
        {    
            _DOCFactory = DocumentBuilderFactory.newInstance();
            _DOCFactory.setIgnoringElementContentWhitespace(true);
            
            _DOCBuilder = _DOCFactory.newDocumentBuilder();
            _DOC        = _DOCBuilder.parse("biblio.xml");
        }
        catch(Exception e) 
        {
            System.out.println (e);
        }
    }
    
    public void Start()
    {
		System.out.println ("------------------------------------------------------------\n");
        Node doc = _DOC.getFirstChild();
		Node root = doc.getNextSibling();
        AddNewNode( root );
        System.out.println ("  -> Elemento aggiunto con successo!");
        System.out.println ("\n------------------------------------------------------------\n");
        WriteXMLFile("./biblio.new.xml");
        System.out.println ("  -> Nuova versione file XML salvata in \"biblio.new.xml\"!");
        System.out.println ("\n------------------------------------------------------------\n");
        NavigateNode(_DOC.getDocumentElement(), 0);
        System.out.println ("\n------------------------------------------------------------");
    }
    
    private String GenerateSpace(int level)
    {
        String t = "     ";
        
        while(level-- > 0)
            t += " ";
        
        return t;
    }
    
    private void NavigateNode(Node nodo, int level) 
    {
        String space = GenerateSpace(level);
    
        switch(nodo.getNodeType()) 
        {
            case Node.ELEMENT_NODE:
                System.out.print(space + "<" + nodo.getNodeName());
                
                NamedNodeMap attributi = nodo.getAttributes();
                if(attributi.getLength() > 0) 
                {
                    for(int i=0; i<attributi.getLength(); i++) 
                    {
                        Attr attributo = (Attr) attributi.item(i);
                        System.out.print(" " + attributo.getNodeName() + "=\"" + attributo.getNodeValue() + "\"");
                    }
                    
                    System.out.print(">\n");
                }
                else System.out.print(">\n");

                NavigateChilds(nodo.getChildNodes(), ++level);
                System.out.println(space + "</" + nodo.getNodeName() + ">");
            break;

            case Node.CDATA_SECTION_NODE:
            case Node.TEXT_NODE:
                Text testo = (Text) nodo;

                if(!testo.getNodeValue().trim().equals(""))
                    System.out.println(space + testo.getNodeValue().trim());
            break;
        }
    }
		 
    private void NavigateChilds(NodeList figli, int level)
    {
        int lev = ++level;
        
        if(figli.getLength() > 0)
            for(int i = 0; i < figli.getLength(); i++)
                NavigateNode(figli.item(i), lev);
    }
    
    private void WriteXMLFile(String filename) 
    {
        try {
            DOMSource source = new DOMSource(_DOC);
 
            File file = new File(filename);
            StreamResult result = new StreamResult(file);
 
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.transform(source, result);
        } 
        catch (TransformerConfigurationException e) {} 
        catch (TransformerException e) {}
    }

    private void AddNewNode( Node root )
    {
        // Nodo: LIBRO
        Node libro = _DOC.createElement("Libro");
        root.appendChild( libro );
        
        // Nodo: CODICE
        Node codice = _DOC.createElement("Codice");
        libro.appendChild( codice );
        Node ctext = _DOC.createTextNode("0103");
        codice.appendChild(ctext);
        
        // Nodo: TITOLO
        Node titolo = _DOC.createElement("Titolo");
        libro.appendChild( titolo );
        Node ttext = _DOC.createTextNode("Design Patterns Elements of Reusable ObjectOriented Software");
        titolo.appendChild(ttext);
        
        Map<String, String> _MapAuthor = new HashMap<String, String>();
        _MapAuthor.put("Erich Gamma", "Principale");
        _MapAuthor.put("Richard Helm", "Coautore");
        _MapAuthor.put("Ralph Johnson", "Coautore");
        _MapAuthor.put("John Vlissides", "Coautore");
        
        Iterator it = _MapAuthor.entrySet().iterator();
        
        while (it.hasNext())
        {
            Map.Entry _map  = (Map.Entry) it.next();
            String key      = (String) _map.getKey();
            String val      = (String) _map.getValue();
            
            // Nodo: AUTORE
            Node autore = _DOC.createElement("Autore");
            libro.appendChild( autore );
            
            Node atext = _DOC.createTextNode(key);
            autore.appendChild(atext);
            
            Element elem = (Element) autore;
            elem.setAttribute("Tipo", val);
        }
    }
}