import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class Esercizio8
{
    public static void main(String[] args)
    {
        XMLReader xmlr = new XMLReader();
        xmlr.Start();
    }
}

class XMLReader
{
    // Membri
    DocumentBuilderFactory _DOCFactory  = null;
    DocumentBuilder _DOCBuilder         = null;
    Document _DOC                       = null;
    
    public XMLReader()
    {
        try
        {    
            _DOCFactory = DocumentBuilderFactory.newInstance();
            _DOCBuilder = _DOCFactory.newDocumentBuilder();
            _DOC        = _DOCBuilder.parse("doc07.xml");
        }
        catch(Exception e) 
        {
            System.out.println (e);
        }
    }
    
    public void Start()
    {
		System.out.println ("------------------------------------------------------------\n");
		Node doc = _DOC.getFirstChild();
		Node root = doc.getNextSibling();
        StepAllFlights(root);
        System.out.println ("------------------------------------------------------------");
    }
    
    private String GetDocType (Node nd) 
    {
		switch(nd.getNodeType())
        {
            case Node.DOCUMENT_NODE:
                return "DOCUMENT_NODE";
            
            case Node.TEXT_NODE:
                return "TEXT_NODE";
            
            case Node.DOCUMENT_TYPE_NODE:
                return "DOCUMENT_TYPE_NODE";
                
            case Node.ELEMENT_NODE:
                return "ELEMENT_NODE";
		}
		
		return "WARNING ->  UNEXPECTED( " + nd.getNodeType() + " )!";
	}

	private void GetFlightValue (Node nd) 
    {
		String ds       = null;
		String cv       = null;
		String dt       = null;
	   
        for (Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
            if(i.getNodeName().equals("destinazione"))
            {
                Node dsn = i.getFirstChild();
                ds       =  ((dsn != null) ? dsn.getNodeValue() : " --");
            }
            
        for (Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
            if(i.getNodeName().equals("volo"))
            {
                Node cvn = i.getFirstChild();
                cv       =  ((cvn != null) ? cvn.getNodeValue() : " --");
            }
            
        for (Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
            if(i.getNodeName().equals("data"))
            {
                Node dtn = i.getFirstChild();
                dt       =  ((dtn != null) ? dtn.getNodeValue() : " --");
            }
            
        System.out.println("\t -> Aeroporto Arrivo: " + ds);
        System.out.println("\t -> Codice Volo: " + cv);
        System.out.println("\t -> Data: " + dt + "\n");
    }
    
    private void StepAllFlights( Node nd )
    {
        for (Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
            if (i.getFirstChild().getFirstChild().getNodeValue().equals("Milano Malpensa"))
                GetFlightValue(i);
    }
}