import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class Esercizio7
{
    public static void main(String[] args)
    {
        XMLReader xmlr = new XMLReader();
        xmlr.Start();
    }
}

class XMLReader
{
    // Membri
    DocumentBuilderFactory _DOCFactory  = null;
    DocumentBuilder _DOCBuilder         = null;
    Document _DOC                       = null;
    
    public XMLReader()
    {
        try
        {    
            _DOCFactory = DocumentBuilderFactory.newInstance();
            _DOCBuilder = _DOCFactory.newDocumentBuilder();
            _DOC        = _DOCBuilder.parse("doc07.xml");
        }
        catch(Exception e) 
        {
            System.out.println (e);
        }
    }
    
    public void Start()
    {
		System.out.println ("------------------------------------------------------------\n");
        GetDocData (_DOC);
        System.out.println ("\n------------------------------------------------------------");
    }
    
    private String GetDocType (Node nd) 
    {
		switch(nd.getNodeType())
        {
            case Node.DOCUMENT_NODE:
                return "DOCUMENT_NODE";
            
            case Node.TEXT_NODE:
                return "TEXT_NODE";
            
            case Node.DOCUMENT_TYPE_NODE:
                return "DOCUMENT_TYPE_NODE";
                
            case Node.ELEMENT_NODE:
                return "ELEMENT_NODE";
		}
		
		return "WARNING ->  UNEXPECTED( " + nd.getNodeType() + " )!";
	}

	private void GetNodeInfo (Node nd, int move) 
    {
		String type     = GetDocType (nd);
		String name     = nd.getNodeName();
		String value    = nd.getNodeValue();
		String tab      = "";
        
        for (int i = 0; i < move ; i++)
            tab += "\t";
        
		System.out.print (tab + " -> " + type + ".Name( " + name + " ):  " + 
                            ((value == null) ? value : " --") + "\n");
	}
    
    private void GetNodeText (Node nd)
    {   
        System.out.println (" ->\tNode.Value:  " + nd.getNodeValue());
    }
    
    private void GetDocData(Document _doc)
    {
        if (_doc == null)
            return;
    
		GetNodeInfo( _doc, 0 );
		
		Node doc = _doc.getFirstChild();
		GetNodeInfo( doc, 0 );
		
		Node root = doc.getNextSibling();
		GetNodeInfo( root, 0 );
        
        StepAll( root, 0 );
        
        System.out.println("\n");
        StepAllText( root );
	}
    
    private void StepAll( Node nd, int move )
    {
        GetNodeInfo( nd, move );
    
        for(Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
            StepAll( i, move + 1 );
    }
    
    private void StepAllText( Node nd )
    {
        if(nd.getNodeType() == Node.TEXT_NODE)
            GetNodeText( nd );
    
        for(Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
            StepAllText( i );
    }
}